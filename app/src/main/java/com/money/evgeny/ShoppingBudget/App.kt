package com.money.evgeny.ShoppingBudget

import android.app.Application
import androidx.room.Room
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.money.evgeny.ShoppingBudget.room.AppDatabase
import io.fabric.sdk.android.Fabric

class App : Application() {

    private lateinit var database: AppDatabase

    //==================================================================================================================

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()

        initCrashlytics()

        // App
        instance = this

        // Database
        database = Room.databaseBuilder(this, AppDatabase::class.java, "shopping_db")
            .build()

    }

    //------------------------------------------------------------------------------------------------------------------

    fun initCrashlytics(){
        // Set up Crashlytics, disabled for debug builds
        val crashlyticsKit = Crashlytics.Builder()
            .core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
            .build()

        Fabric.with(this, crashlyticsKit)
    }

    fun getDatabase() = database

    fun getDao() = database.listDao()

}

