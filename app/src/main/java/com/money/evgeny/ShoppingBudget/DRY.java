package com.money.evgeny.ShoppingBudget;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.button.MaterialButton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

// класс с методами, которые используются больше 1-2 раз в коде приложения (DRY - don`t repeat yourself)
public class DRY {

    public static boolean exitToTwiceBackpressed = false;                                           // выход по двойному нажатию кнопки Назад
    public static ProgressDialog pd;


    // Методы общие для любых приложений ===========================================================

    //region ProgressDialog Start/Stop

    public static void pdStart(Context context) {
        pd = new ProgressDialog(context);
        pd.setMessage("Подождите...");
//        pd.setCancelable(false);
        pd.show();
    }

    public static void pdStop() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    //endregion

    //region Open/Close keyboard

    // скрывает или показывает клаву в зависимости от ее состояния (скрыта - показал, показана - скрыл)
    public static void hideOrShowKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); // hide
        } else {
            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY); // show
        }
    }

    //Скрыть клавиатуру для Activity
    public static void hideKeyboardForActivity(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //Скрыть клавиатуру для View
    public static void hideKeyboardForView(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //endregion

    /*

    //region Декоратор масок от Tinkoff (только с цифрами)

    // маска для номера телефона
    public static void setMaskPhone(EditText editText) {
        Slot[] slots = new UnderscoreDigitSlotsParser().parseSlots("_ (___) ___-__-__");
        MaskImpl mask = MaskImpl.createTerminated(slots);
        FormatWatcher watcher = new MaskFormatWatcher(mask);
        watcher.installOn(editText);
    }

    // очищает маску для телефона от мишуры типа скобок и т.д, оставляя лишь данные
    public static String cleanMaskPhone(String string) {
        Slot[] slots = new PhoneNumberUnderscoreSlotsParser().parseSlots("_ (___) ___-__-__");
        MaskImpl inputMask = MaskImpl.createTerminated(slots);
        inputMask.insertFront(string);
        return inputMask.toUnformattedString();

    }

    //endregion
*/
    //region DatePicker, TimePicker

    public static void showDatePicker(Context context, final View myView, Boolean setMinDateNow) {
        Calendar now = Calendar.getInstance();
        int day = now.get(Calendar.DAY_OF_MONTH);
        int month = now.get(Calendar.MONTH);
        int year = now.get(Calendar.YEAR);

        DatePickerDialog dpd = new DatePickerDialog(context, TimePickerDialog.THEME_HOLO_LIGHT, new OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                setDateForView(myView, returnDate(year, month, dayOfMonth));
            }
        }, year, month, day);

        // установка минимальной даты с текущей
        if (setMinDateNow) {
            dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);                      // отключение всех прошедших дней
        }

        // апрет на редоктирование с клавиатуры
        dpd.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        dpd.show();

    }

    public static void showTimePicker(Context context, final View myView) {
        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);

        TimePickerDialog tpd = new TimePickerDialog(context, TimePickerDialog.THEME_HOLO_LIGHT, (view, hourOfDay, minute1) -> setDateForView(myView, returnTime(hourOfDay, minute1)), hour, minute, true);

        tpd.show();

    }

    // формирование даты в конечный фид (00/00/0000)
    private static String returnDate(int year, int monthOfYear, int dayOfMonth) {
        return dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
    }

    // формирование время в конечный фид (00:00)
    private static String returnTime(int hourOfDay, int minute) {
        String time;

        if (minute == 0) {
            // по дефолту пикер при 00 ставит просто 0 (прмиер 14:0), фикс - это прибавить 0 самостоятельно
            String modMinute = "00";
            time = hourOfDay + ":" + modMinute;
        } else if (minute < 10) {
            time = hourOfDay + ":" + ("0" + minute);
        } else time = hourOfDay + ":" + minute;


        return time;
    }

    // установка даты в компонент
    private static void setDateForView(View myView, String date_time) {
        if (myView instanceof MaterialButton || myView instanceof Button) {
            ((Button) myView).setText(date_time);
        } else if (myView instanceof TextView) {
            ((TextView) myView).setText(date_time);
        }
    }

    //endregion

    // конвертация bitmap-картинки в base64 без ухудшением качества
    public static String convertBitmapToBase64(File imageFile) {
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

        // размер картинки в килобайтах
        long imageSizeKb = (imageFile.length() / 1000);

        // если размер меньше 3 Мб то загружаю на сервер, иначе обрезаю качество
        byte[] imgByte;
        if (imageSizeKb < 1500) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            imgByte = stream.toByteArray();
        } else {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
            imgByte = stream.toByteArray();
        }

        return Base64.encodeToString(imgByte, Base64.DEFAULT);
    }

    /*
        // спросить разрешение на использование телефона
        public static void askPhonePermission(final Context context) {
            Dexter.withActivity((Activity) context)
                    .withPermission(Manifest.permission.CALL_PHONE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            // позвонить Re.parts
                            context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "83912054504", null)));
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        }

        // проверка подключения к интернету
        public static boolean checkNetworkAvailable(Context context) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
    */

    // открыть ссылку в браузере
    public static void openUrlInBrowser(Context context, String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    // выход по двойному нажатию кнопки Назад
    public static void exitOnTwiceBackPressed(Context context) {
        exitToTwiceBackpressed = true;
        Toast.makeText(context, "Нажмите еще раз, чтобы выйти", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> exitToTwiceBackpressed = false, 2000);

        /* это вставить в Activity/Fragment

        @Override
        public void onBackPressed() {
        if (DRY.exitToTwiceBackpressed) {
            super.onBackPressed();
            return;
        }

        DRY.exitOnTwiceBackPressed(this);
        }

        */

    }

    public static double roundDouble(double value){
        return Math.round(100.0 * value) / 100.0;
    }

    public static String getCurrentDate(){
        Date calendar = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        return df.format(calendar);
    }

    public static void getAppVersion(TextView textView) {
        /*String versionName;
        try {
            versionName = App.getInstance().getResources().getString(R.string.about_tv_version) + " " + App.getInstance().getApplicationContext().getPackageManager().getPackageInfo(App.getInstance().getApplicationContext().getPackageName(), 0).versionName;
            textView.setText(versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    public static void rateInGooglePlay(Context context) {
        try {
            Intent intent_GooglePlay = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName()));
            context.startActivity(intent_GooglePlay);
        } catch (android.content.ActivityNotFoundException exception) {
            // Перехват ошибки - если если GooglePlay не установлен и отрытие в обычном браузере
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    // поделиться приложением
    public static void shareApp(Context context) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);                      //Intent с параметром "action" = "отправка"
        sharingIntent.setType("text/plain");
        String shareBody = "Попробуй приложение для подсчета расходов." + "\n" + "Ссылка на приложение: https://play.google.com/store/apps/details?id=com.money.evgeny.ShoppingBudget";
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Приложение: \"Бюджет покупок\""); //Вроде только для Gmail (там есть окошечко тема письма - вот туда и вставляется)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Поделиться через..."));  //Заголовок всплывающего алерта с вариантами через что можно поделиться
    }




    // Методы общие только для этого приложения-----------------------------------------------------



}
