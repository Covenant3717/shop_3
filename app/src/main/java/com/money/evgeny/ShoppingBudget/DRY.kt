package com.money.evgeny.ShoppingBudget

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*

fun Context.liteToast(text: String?, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, duration).show()
}

fun getAppVersion(): String =
    "${App.instance.resources.getString(R.string.about_tv_version)} ${App.instance.applicationContext.packageManager.getPackageInfo(
        App.instance.applicationContext.packageName,
        0
    ).versionName}"


fun Context.rateInGooglePlay() {
    try {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${this.packageName}")))
    } catch (exception: android.content.ActivityNotFoundException) {
        // Перехват ошибки - если если GooglePlay не установлен, то открыть в обычном браузере
        this.startActivity(
            Intent(
                Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=${this.packageName}")
            )
        )
    }
}

fun Context.shareApp() {
    val sharingIntent = Intent(Intent.ACTION_SEND)
    sharingIntent.type = "text/plain"

    val shareBody = """
    |Попробуй приложение для подсчета расходов.
    |Ссылка на приложение: https://play.google.com/store/apps/details?id=com.money.evgeny.ShoppingBudget""".trimMargin()

    //Вроде только для Gmail (там есть окошечко тема письма - вот туда и вставляется)
    sharingIntent.putExtra(
        Intent.EXTRA_SUBJECT,
        "Приложение: \"Бюджет покупок\""
    )

    //Заголовок всплывающего алерта с вариантами через что можно поделиться
    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
    this.startActivity(
        Intent.createChooser(
            sharingIntent,
            "Поделиться через..."
        )
    )
}

fun roundDouble(value: Double): Double =
    BigDecimal(value).setScale(2, RoundingMode.HALF_EVEN).toDouble()

fun getCurrentDate(): String {
    val calendar = Calendar.getInstance().time
    val df = SimpleDateFormat("dd.MM.yyyy")
    return df.format(calendar)
}