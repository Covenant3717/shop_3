package com.money.evgeny.ShoppingBudget.room

import androidx.room.Database
import androidx.room.RoomDatabase

/*
 Это основной класс по работе с базой данных.
    - предоставляет доступ к самому файлу БД;
    - доступ к версии БД;
    - здесь же указываем все сущности используемые в БД
*/

@Database(entities = [ProductList::class, Product::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun listDao(): ListDao
}
