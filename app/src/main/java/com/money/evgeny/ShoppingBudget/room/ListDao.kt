package com.money.evgeny.ShoppingBudget.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update


// Dao (Data access object) - объект описывающий методы рвботы с базой данных
@Dao
interface ListDao {

    /* Запросы для таблицы list_table */

    @Query("SELECT * FROM lists_table")
    fun getAllLists(): List<ProductList>

    @Query("SELECT * FROM lists_table WHERE id = :id")
    fun getListById(id: Long): ProductList

    @Insert
    fun insertList(productList: ProductList): Long

    @Update
    fun updateList(productList: ProductList)

    @Query("DELETE FROM lists_table WHERE id = :id")
    fun deleteList(id: Long)


    /* Запросы для таблицы products_table */

    @Query("SELECT * FROM products_table WHERE list_id = :id")
    fun getAllProductsById(id: Long): List<Product>

    @Insert
    fun insertProduct(product: Product): Long

}
