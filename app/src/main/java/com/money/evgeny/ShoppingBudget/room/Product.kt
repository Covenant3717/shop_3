package com.money.evgeny.ShoppingBudget.room

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(
    tableName = "products_table",
    foreignKeys = [ForeignKey(
        entity = ProductList::class,
        parentColumns = ["id"],
        childColumns = ["list_id"],
        onDelete = CASCADE
    )]
)
data class Product(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    var list_id: Long?,
    var name: String = "",
    var cost: Double = 0.0,
    @Ignore var hasFocus: Boolean = true
) {

    constructor() : this(null, null, "", 0.0)
    constructor(list_id: Long, name: String, cost: Double) : this(null, list_id, name, cost)
    constructor(name: String, cost: Double) : this(null, null, name, cost)
}
