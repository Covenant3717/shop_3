package com.money.evgeny.ShoppingBudget.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "lists_table")
data class ProductList(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    var name: String,
    var date: String,
    var balance: Double,
    var costs: Double,
    var residue: Double
) {

    constructor() : this(null, "", "", 0.0, 0.0, 0.0)
    constructor(name: String, date: String, balance: Double, costs: Double, residue: Double) : this(
        null,
        name,
        date,
        balance,
        costs,
        residue
    )

}
