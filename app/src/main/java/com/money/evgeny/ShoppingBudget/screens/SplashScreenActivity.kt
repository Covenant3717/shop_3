package com.money.evgeny.ShoppingBudget.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.money.evgeny.ShoppingBudget.screens.main.MainActivity
import android.content.Intent


class SplashScreenActivity : AppCompatActivity() {

    //==================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }


}
