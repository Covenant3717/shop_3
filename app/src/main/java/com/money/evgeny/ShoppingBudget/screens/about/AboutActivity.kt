package com.money.evgeny.ShoppingBudget.screens.about

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.jaeger.library.StatusBarUtil
import com.money.evgeny.ShoppingBudget.R
import com.money.evgeny.ShoppingBudget.getAppVersion
import com.money.evgeny.ShoppingBudget.rateInGooglePlay
import com.money.evgeny.ShoppingBudget.shareApp
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity(), View.OnClickListener {

    //==================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        initMain()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.about_fab_back -> {
                finish()
            }
            R.id.about_fab_rate -> {
                rateInGooglePlay()
            }
            R.id.about_fab_share -> {
                shareApp()
            }
        }
    }

    //==================================================================================================================

    private fun initMain() {
        StatusBarUtil.setTransparent(this)

        about_tv_version.text = getAppVersion()

        about_fab_back.setOnClickListener(this)
        about_fab_rate.setOnClickListener(this)
        about_fab_share.setOnClickListener(this)
    }

}