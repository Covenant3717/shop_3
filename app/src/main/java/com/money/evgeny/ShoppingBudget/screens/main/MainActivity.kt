package com.money.evgeny.ShoppingBudget.screens.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.jaeger.library.StatusBarUtil
import com.money.evgeny.ShoppingBudget.*
import com.money.evgeny.ShoppingBudget.room.ProductList
import com.money.evgeny.ShoppingBudget.screens.about.AboutActivity
import com.money.evgeny.ShoppingBudget.screens.main.adapters.SavedListsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.alert_save.view.*


class MainActivity : AppCompatActivity(), View.OnClickListener, View.OnLongClickListener {

    private lateinit var appUpdateManager: AppUpdateManager

    private val presenter = MainPresenter(this)

    private var bottomSheetDialog: BottomSheetDialog? = null

    //==================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initMain()
    }

    override fun onPause() {
        super.onPause()

        bottomSheetDialog?.dismiss()
    }

    override fun onResume() {
        super.onResume()
        appUpdateManager.appUpdateInfo.addOnSuccessListener {
            if (it.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                appUpdateManager.startUpdateFlowForResult(
                    it,
                    AppUpdateType.IMMEDIATE,
                    this,
                    REQUEST_CODE_FLEXI_UPDATE
                )
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachView()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.main_fab_add -> {
                presenter.addProduct()
            }

            R.id.main_fab_save -> {
                presenter.btnFabSave()
            }
        }
    }

    override fun onLongClick(v: View?): Boolean {
        when (v?.id) {
            R.id.main_fab_add -> {
                presenter.fullClear()
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_FLEXI_UPDATE) {
            when (resultCode) {

                //  обработчик кнопки "Обновить"
                Activity.RESULT_OK -> {}

                //  обработчик кнопки "Отмена"
                Activity.RESULT_CANCELED -> {}

                // обработчик ошибки обновления
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {}
            }
        }
    }


    // init ============================================================================================================

    private fun checkAppUpdatesInGooglePlay() {
        appUpdateManager = AppUpdateManagerFactory.create(this)

        // проверить доступность обновления в Google play
        appUpdateManager.appUpdateInfo.addOnSuccessListener {
            if (it.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {

                // показать диалог с доступным обновлением (запуск/отправка intent)
                appUpdateManager.startUpdateFlowForResult(
                    it,
                    AppUpdateType.FLEXIBLE,        //  HERE specify the type of update flow you want
                    this,                           //  the instance of an activity
                    REQUEST_CODE_FLEXI_UPDATE
                )
            }

        }
            .addOnFailureListener { e ->
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
    }

    private fun initMain() {
        StatusBarUtil.setTransparent(this)

        presenter.initCostsLiveData(this)
        presenter.initFabSaveLiveData(this)

        presenter.listId = intent.getLongExtra("listId", 0)

        main_fab_save.setOnClickListener(this)
        main_fab_add.setOnClickListener(this)
        main_fab_add.setOnLongClickListener(this)

        presenter.initRecyclerProducts(this, main_recycler_products)
        initAppBar()

        when (presenter.listId) {
            0L -> {
                main_group.visibility = View.GONE
                presenter.initEditBalance(main_ed_balance)
                presenter.addProduct()
            }
            else -> {
                main_group.visibility = View.VISIBLE
                main_fab_save.visibility = View.INVISIBLE
                presenter.loadList(presenter.listId, presenter.productAdapter)
            }
        }

        initAdView()

        checkAppUpdatesInGooglePlay()
    }

    private fun initAppBar() {
        // icon navigation
        main_appbar.navigationIcon = getDrawable(R.drawable.ic_main_bar_settings)
        main_appbar.setNavigationOnClickListener {
            startActivity(Intent(this, AboutActivity::class.java))
        }

        // action menu
        main_appbar.replaceMenu(R.menu.bar_menu)
        when {
            presenter.listId > 0 -> {
                // иконку списков скрыть
                main_appbar.menu.getItem(0).isVisible = false

                // иконки Редактировать и Удалить показать
                main_appbar.menu.getItem(1).isVisible = true
                main_appbar.menu.getItem(2).isVisible = true
            }
        }

        main_appbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.bar_menu_lists -> presenter.btnShowBottomSheetDialog()
                R.id.bar_menu_rename -> presenter.btnRenameList()
                R.id.bar_menu_delete -> presenter.btnDeleteList()
            }
            true
        }
    }

    private fun initAdView() {
        MobileAds.initialize(this, resources.getString(R.string.admob_id_app))

        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder()
            .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
            .addTestDevice(resources.getString(R.string.device_id))
            .build()
        mAdView.loadAd(adRequest)

        mAdView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                mAdView.visibility = View.VISIBLE
            }

            override fun onAdFailedToLoad(error: Int) {
                mAdView.visibility = View.GONE
            }
        }
    }


    fun getListData(): HashMap<String, Any> {
        val data = HashMap<String, Any>()

        when (presenter.listId) {
            0L -> {
            }
            else -> {
                data[MainPresenter.NAME] = main_name_list.text.toString()
                data[MainPresenter.DATE] = main_date_list.text.toString()
            }
        }

        val balance = main_ed_balance.text.toString()
        val costs = main_tv_costs.text.toString()
        val residue = main_tv_residue.text.toString()

        data[MainPresenter.BALANCE] = if (balance == "") 0.0 else balance.toDouble()
        data[MainPresenter.COSTS] = if (costs == "") 0.0 else costs.toDouble()
        data[MainPresenter.RESIDUE] = if (residue == "") 0.0 else residue.toDouble()
        data[MainPresenter.LIST_PRODUCTS] = presenter.productAdapter.getProductList()

        return data
    }

    fun showBottomSheetDialog(allLists: List<ProductList>) {
        bottomSheetDialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
        val bottomSheetView = layoutInflater.inflate(R.layout.bottom_sheet_saved_lists, null)
        bottomSheetDialog?.setContentView(bottomSheetView)
        bottomSheetDialog?.show()

        val recyclerSaved = bottomSheetView.findViewById<RecyclerView>(R.id.bottom_sheet_recycler)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, true)
        layoutManager.stackFromEnd = true
        recyclerSaved?.layoutManager = layoutManager

        recyclerSaved.adapter = SavedListsAdapter(this@MainActivity, allLists)
    }

    fun showDialogSaveList() {
        val v = layoutInflater.inflate(R.layout.alert_save, null, false)
        val dialog = AlertDialog.Builder(this, R.style.CustomDialog)
            .setView(v)
            .show()

        v?.alert_save_btn_save?.setOnClickListener {
            val textInEdit = v.alert_save_ed_name.text.toString()
            presenter.btnOkFromSaveListDialog(dialog, textInEdit)
        }

        v?.alert_save_btn_cancel?.setOnClickListener { dialog.dismiss() }
    }

    fun showSnackbarConfirmDeleteList() {
        Snackbar.make(main_coordinator, resources.getString(R.string.snackbar_delete_list_hint), Snackbar.LENGTH_SHORT)
            .setAnchorView(main_fab_add)
            .setAction(resources.getString(R.string.snackbar_delete_list_btn)) {
                presenter.btnSnackbarConfirmDeleteList(presenter.listId)
            }
            .show()
    }

    fun showDialogRenameList() {
        val v = layoutInflater.inflate(R.layout.alert_save, null, false)

        val dialog = AlertDialog.Builder(this, R.style.CustomDialog)
            .setView(v)
            .show()

        v?.alert_save_ed_name?.setText(main_name_list.text)

        v?.alert_save_btn_save?.setOnClickListener {
            val textInEdit = v.alert_save_ed_name.text.toString()
            presenter.btnOkFromRenameListDialog(dialog, presenter.listId, textInEdit)
        }

        v?.alert_save_btn_cancel?.setOnClickListener { dialog.dismiss() }

    }

    fun toastListNameEmpty() = liteToast(resources.getString(R.string.alert_toast_empty_name))

    fun toastListSaved() = liteToast(resources.getString(R.string.alert_toast_saved))

    fun toastFullClear() = liteToast(resources.getString(R.string.main_toast_full_clear))


}


