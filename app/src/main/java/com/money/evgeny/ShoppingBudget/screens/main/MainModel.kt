package com.money.evgeny.ShoppingBudget.screens.main

import com.money.evgeny.ShoppingBudget.App
import com.money.evgeny.ShoppingBudget.room.Product
import com.money.evgeny.ShoppingBudget.room.ProductList
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.ArrayList

class MainModel {

    private val listDao = App.instance.getDao()

    //==================================================================================================================

    fun getAllLists(): List<ProductList> {
        return listDao.getAllLists()
    }

    fun getListById(id: Long): ProductList {
        return listDao.getListById(id)
    }

    fun insertListInDatabase(list: ProductList, listProduct: List<Product>): Long {
        val listId = listDao.insertList(list)
        for (item in listProduct) {
            val product = Product(listId, item.name, item.cost)
            listDao.insertProduct(product)
        }
        return listId
    }

    fun deleteListFromDatabase(listId: Long) {
        listDao.deleteList(listId)
    }

    fun updateList(list: ProductList) {
        listDao.updateList(list)
    }

    fun getAllProductsById(listId: Long): List<Product> {
        return listDao.getAllProductsById(listId)
    }


}