package com.money.evgeny.ShoppingBudget.screens.main

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.jaeger.library.StatusBarUtil
import com.money.evgeny.ShoppingBudget.R
import com.money.evgeny.ShoppingBudget.getCurrentDate
import com.money.evgeny.ShoppingBudget.liteToast
import com.money.evgeny.ShoppingBudget.room.Product
import com.money.evgeny.ShoppingBudget.room.ProductList
import com.money.evgeny.ShoppingBudget.roundDouble
import com.money.evgeny.ShoppingBudget.screens.about.AboutActivity
import com.money.evgeny.ShoppingBudget.screens.main.MainPresenter.Companion.BALANCE
import com.money.evgeny.ShoppingBudget.screens.main.MainPresenter.Companion.COSTS
import com.money.evgeny.ShoppingBudget.screens.main.MainPresenter.Companion.LIST_PRODUCTS
import com.money.evgeny.ShoppingBudget.screens.main.MainPresenter.Companion.RESIDUE
import com.money.evgeny.ShoppingBudget.screens.main.adapters.ProductsAdapter
import com.money.evgeny.ShoppingBudget.screens.main.adapters.ProductsTouchHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainPresenter(private var view: MainActivity?) {

    private val model = MainModel()

    companion object {
        const val NAME = "listName"
        const val DATE = "listDate"
        const val BALANCE = "balance"
        const val COSTS = "costs"
        const val RESIDUE = "residue"
        const val LIST_PRODUCTS = "listProducts"

    }

    var listId: Long = 0

    private val costLiveData = MutableLiveData<Double>()
    val fabSsveVisibleLiveData = MutableLiveData<Boolean>()

    lateinit var productAdapter: ProductsAdapter
    private lateinit var layoutManager: LinearLayoutManager

    private var balance: Double = 0.0
    private var costs: Double = 0.0
    private var residue: Double = 0.0


    //==================================================================================================================

    fun detachView() {
        listId = 0
        fabSsveVisibleLiveData.value = false
        view = null
    }

    //==================================================================================================================

    fun initEditBalance(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val strBalance = s.toString()
                val strCosts = view?.main_tv_costs?.text.toString()
                val strResidue = view?.main_tv_residue?.text.toString()

                balance = if (strBalance.isEmpty() || strBalance == ".") {
                    0.0
                } else strBalance.toDouble()

                costs = if (strCosts.isEmpty()) {
                    0.0
                } else productAdapter.getFullCost()

                residue = if (strResidue.isEmpty()) {
                    0.0
                } else balance - costs

                view?.main_tv_residue?.text = roundDouble(balance - costs).toString()

                if (listId > 0) {
                    fabSsveVisibleLiveData.postValue(true)
                }
            }
        })
    }

    fun initCostsLiveData(lifecycleOwner: LifecycleOwner) {
        costLiveData.observe(lifecycleOwner, Observer { value ->
            costs = value
            view?.main_tv_costs?.text = roundDouble(costs).toString()

            residue = balance - value
            view?.main_tv_residue?.text = roundDouble(residue).toString()
        })
    }

    fun initFabSaveLiveData(lifecycleOwner: LifecycleOwner) {
        fabSsveVisibleLiveData.observe(lifecycleOwner, Observer {
            if (listId > 0) {
                when (it) {
                    true -> {
                        view?.main_fab_save?.visibility = View.VISIBLE
                    }
                    false -> {
                        view?.main_fab_save?.visibility = View.INVISIBLE
                    }
                }
            }
        })
    }

    fun initRecyclerProducts(context: Context, recyclerView: RecyclerView) {
        layoutManager = LinearLayoutManager(context)
        layoutManager.reverseLayout = true
        layoutManager.stackFromEnd = true
        recyclerView.layoutManager = layoutManager

        productAdapter = ProductsAdapter(context, costLiveData, fabSsveVisibleLiveData)
        recyclerView.adapter = productAdapter

        val productsTouchHelper = ProductsTouchHelper(productAdapter, costLiveData, fabSsveVisibleLiveData)
        val itemTouchHelper = ItemTouchHelper(productsTouchHelper)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }


    fun btnFabSave() {
        if (listId == 0L) {
            view?.showDialogSaveList()
        } else {
            saveChangesOpenedList()
        }
    }

    fun btnOkFromSaveListDialog(dialog: AlertDialog, name: String) {
        when (name) {
            "" -> view?.toastListNameEmpty()

            else -> {
                val id = GlobalScope.async {
                    val list = ProductList(name, getCurrentDate(), balance, costs, residue)
                    model.insertListInDatabase(list, productAdapter.getProductList())
                }

                GlobalScope.launch(Dispatchers.Main) {
                    if (listId > 0) {
                        listId = id.await()
                    }

                    dialog.dismiss()
                    view?.toastListSaved()
                }
            }
        }
    }

    fun btnShowBottomSheetDialog() {
        val alLists = GlobalScope.async {
            model.getAllLists()
        }

        GlobalScope.launch(Dispatchers.Main) {
            view?.showBottomSheetDialog(alLists.await())
        }
    }

    fun btnDeleteList() {
        view?.showSnackbarConfirmDeleteList()
    }

    fun btnSnackbarConfirmDeleteList(listId: Long) {
        GlobalScope.launch {
            model.deleteListFromDatabase(listId)
        }
        view?.finish()
    }

    fun btnRenameList() {
        view?.showDialogRenameList()
    }

    fun btnOkFromRenameListDialog(dialog: Dialog, listId: Long, name: String) {
        when (name) {
            "" -> view?.toastListNameEmpty()
            else -> {

                val list = GlobalScope.async {
                    model.getListById(listId)
                }

                GlobalScope.launch {
                    list.await().name = name
                    model.updateList(list.await())
                }

                view?.main_name_list?.text = name
                dialog.dismiss()
                view?.toastListSaved()
            }
        }
    }

    fun loadList(listId: Long, productAdapter: ProductsAdapter) {
        val list = GlobalScope.async {
            model.getListById(listId)
        }
        val products = GlobalScope.async {
            model.getAllProductsById(listId)
        }

        GlobalScope.launch {
            balance = list.await().balance
            costs = list.await().costs
            residue = list.await().residue

            view?.main_name_list?.text = list.await().name
            view?.main_date_list?.text = list.await().date
            view?.main_ed_balance?.setText(if (list.await().balance == 0.0) "" else list.await().balance.toInt().toString())
            view?.main_tv_costs?.text = if (list.await().costs == 0.0) 0.toString() else list.await().costs.toString()
            view?.main_tv_residue?.text =
                if (list.await().residue == 0.0) 0.toString() else list.await().residue.toString()

            products.await().forEach { item ->
                productAdapter.addItem(item)
            }

            view?.main_ed_balance?.let { initEditBalance(it) }
        }
    }

    private fun saveChangesOpenedList() {
        GlobalScope.launch {
            model.deleteListFromDatabase(listId)
        }
        val data = view?.getListData()

        val name = data?.get(NAME) as String
        val date = data[DATE] as String
        val balance = data[BALANCE] as Double
        val costs = data[COSTS] as Double
        val residue = data[RESIDUE] as Double
        val listProducts = data[LIST_PRODUCTS] as List<Product>
        val id = GlobalScope.async {
            val list = ProductList(name, date, balance, costs, residue)
            model.insertListInDatabase(list, listProducts)
        }

        GlobalScope.launch(Dispatchers.Main) {
            listId = id.await()
            view?.toastListSaved()
            view?.main_fab_save?.visibility = View.INVISIBLE
        }
    }

    fun addProduct() {
        val product = Product()
        productAdapter.addItem(product)
        layoutManager.scrollToPosition(productAdapter.itemCount - 1)

        if (listId > 0) {
            fabSsveVisibleLiveData.value = true
        }
    }

    fun fullClear() {
        balance = 0.0
        costs = 0.0
        residue = 0.0

        view?.main_ed_balance?.setText("")
        view?.main_tv_costs?.text = "0"
        view?.main_tv_residue?.text = "0"

        productAdapter.fullClear()

        view?.toastFullClear()

        addProduct()
    }




}