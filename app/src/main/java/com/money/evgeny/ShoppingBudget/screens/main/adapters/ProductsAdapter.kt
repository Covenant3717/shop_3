package com.money.evgeny.ShoppingBudget.screens.main.adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.money.evgeny.ShoppingBudget.R
import com.money.evgeny.ShoppingBudget.room.Product
import com.money.evgeny.ShoppingBudget.roundDouble
import java.util.*

class ProductsAdapter(
    private val _context: Context,
    private val costLiveData: MutableLiveData<Double>,
    private val fabSsveVisibleLiveData: MutableLiveData<Boolean>,
    private var list: ArrayList<Product> = ArrayList(),
    private val viewType: Int = 1
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val PRODUCT_TYPE: Int = 1
    }

    //==================================================================================================================

    override fun getItemViewType(position: Int): Int {
        when (viewType) {
            PRODUCT_TYPE -> return PRODUCT_TYPE
        }

        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        lateinit var v: View
        
        when (viewType) {
            PRODUCT_TYPE -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.item_main_product, parent, false)
            }
        }

        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val current = list[position]

        when (holder.itemViewType) {
            PRODUCT_TYPE -> {
                (holder as MyViewHolder).bind(current)
            }
        }
    }

    override fun getItemCount(): Int = list.size

    //------------------------------------------------------------------------------------------------------------------

    private inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val edName = itemView.findViewById<TextView>(R.id.main_item_ed_product)
        private val edCost = itemView.findViewById<TextView>(R.id.main_item_ed_cost)

        fun bind(current: Product) {
            if (current.hasFocus) {
                edName.requestFocus()
            }
            current.hasFocus = false

            edName?.text = current.name
            edCost?.text = if (current.cost == 0.0) "" else current.cost.toString()

            edName.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    list[adapterPosition].name = s.toString()

                    fabSsveVisibleLiveData.value = true
                }
            })

            edCost.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    list[adapterPosition].cost =
                        if (s.toString().isEmpty() || s.toString() == "" || s.toString() == ".") 0.0 else s.toString().toDouble()
                    costLiveData.value = roundDouble(getFullCost())

                    fabSsveVisibleLiveData.value = true
                }
            })
        }

    }

    //==================================================================================================================

    fun getFullCost(): Double {
        var fullCost = 0.0

        for (product in list) {
            fullCost += product.cost
        }

        return fullCost
    }

    fun getProductList(): List<Product> {
        return list
    }

    fun addItem(product: Product) {
        list.add(product)
        notifyItemInserted(itemCount - 1)
    }

    fun deleteItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    fun moveItem(fromPosition: Int, toPosition: Int): Boolean {
        // меод спизжен из туториала
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(list, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(list, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)

        return true
    }

    fun fullClear() {
        list.clear()
        notifyDataSetChanged()
    }


}
