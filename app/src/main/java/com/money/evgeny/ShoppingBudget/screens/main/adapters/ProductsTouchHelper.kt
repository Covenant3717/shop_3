package com.money.evgeny.ShoppingBudget.screens.main.adapters

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.money.evgeny.ShoppingBudget.roundDouble

class ProductsTouchHelper(
    private val adapter: ProductsAdapter,
    private val costLiveData: MutableLiveData<Double>,
    private val fabSsveVisibleLiveData: MutableLiveData<Boolean>
    ) : ItemTouchHelper.Callback() {

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        adapter.moveItem(viewHolder.adapterPosition, target.adapterPosition)
        fabSsveVisibleLiveData.value = true
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        adapter.deleteItem(position)
        costLiveData.value = roundDouble(adapter.getFullCost())
        fabSsveVisibleLiveData.value = true

    }

}