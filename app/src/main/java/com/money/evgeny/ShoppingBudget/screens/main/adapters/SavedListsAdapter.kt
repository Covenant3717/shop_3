package com.money.evgeny.ShoppingBudget.screens.main.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.money.evgeny.ShoppingBudget.R
import com.money.evgeny.ShoppingBudget.room.ProductList
import com.money.evgeny.ShoppingBudget.screens.main.MainActivity
import java.util.*

class SavedListsAdapter(
    private val context: Context,
    private var list: List<ProductList> = ArrayList()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    //==================================================================================================================

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_bottom_sheet, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val current = list[position]

        (holder as MyViewHolder).bind(current)
    }

    override fun getItemCount(): Int = list.size

    //==================================================================================================================

    private inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val container = item.findViewById<ConstraintLayout>(R.id.bottom_item_container)
        private val tvName = item.findViewById<TextView>(R.id.bottom_item_name)
        private val tvDate = item.findViewById<TextView>(R.id.bottom_item_date)
        private val tvBalance = item.findViewById<TextView>(R.id.bottom_item_balance)

        fun bind(current: ProductList) {
            container.setOnClickListener {
                val intent = Intent(context, MainActivity::class.java)
                intent.putExtra("listId", current.id)
                context.startActivity(intent)
            }

            tvName?.text = current.name
            tvDate?.text = current.date
            tvBalance?.text = current.balance?.toInt().toString()
        }
    }

}
